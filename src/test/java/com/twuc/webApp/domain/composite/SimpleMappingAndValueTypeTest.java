package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.ClosureValue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.twuc.webApp.domain.JpaTestBase;
import org.springframework.dao.DataIntegrityViolationException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SimpleMappingAndValueTypeTest extends JpaTestBase{
    @Autowired
    private UserProfileRepository userProfileRepository;
    @Autowired
    private CompanyProfileRepository companyProfileRepository;

    @Test
    void should_get_and_save_UserProfile_value() {
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(em -> {
            final UserProfile profile =
                    userProfileRepository.save(new UserProfile("Chdd", "Huii"));
            expectedId.setValue(profile.getId());
        });

        run(em -> {
            final UserProfile profile =
                    userProfileRepository.findById(expectedId.getValue()).orElseThrow(
                            RuntimeException::new);

            assertEquals("Chdd", profile.getAddressCity());
            assertEquals("Huii", profile.getAddressStreet());
        });
    }
    @Test
    void should_get_and_save_CompanyProfile_value() {
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(em -> {
            final CompanyProfile companyProfile =
                    companyProfileRepository.save(new CompanyProfile("Chengdu", "Huayang"));
            expectedId.setValue(companyProfile.getId());
        });

        run(em -> {
            final CompanyProfile companyProfile =
                    companyProfileRepository.findById(expectedId.getValue()).orElseThrow(
                            RuntimeException::new);

            assertEquals("Chengdu", companyProfile.getCity());
            assertEquals("Huayang", companyProfile.getStreet());
        });
    }
    @Test
    void should_fail_if_user_profile_addressCity_is_null() {
        run(em -> {
            final UserProfile userProfile = new UserProfile(null, "Huii");
            assertThrows(DataIntegrityViolationException.class, () -> {
                userProfileRepository.save(userProfile);
                userProfileRepository.flush();
            });
        });
    }
}
