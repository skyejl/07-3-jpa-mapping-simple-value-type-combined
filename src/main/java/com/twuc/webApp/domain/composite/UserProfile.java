package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class UserProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false,length = 128)
    private String addressCity;
    @Column(nullable = false,length = 128)
    private String addressStreet;

    public UserProfile() {
    }

    public UserProfile(String addressCity, String addressStreet) {
        this.addressCity = addressCity;
        this.addressStreet = addressStreet;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public Long getId() {
        return id;
    }

    public String getAddressStreet() {
        return addressStreet;
    }
}
